# go-ctk

A Go toolkit for composing GitLab CI components.

## Description

This is an experiment to explore building CI components
programatically. Writing bash scripts in YAML can get
overly complex quickly, which makes it hard to test
and even harder to understand if you're not the maintainer
of the component. The `go-ctk` library hopes to fix these
pain points by providing a toolkit that allows you to
build rich CI components using the Go programming language.
