package main

import (
	"log"

	"gitlab.com/hacks4oats/go-ctk/pkg/command"
)

func main() {
	cmdOne, err := command.NewCommand([]string{"echo", "testing command number one"})
	if err != nil {
		log.Fatal(err)
	}
	_, _, err = cmdOne.Run()
	if err != nil {
		log.Fatal(err)
	}
	cmdTwo, err := command.NewCommand([]string{"echo", "testing command number two with autocollapse"}, command.WithAutoCollapse(true))
	if err != nil {
		log.Fatal(err)
	}
	_, _, err = cmdTwo.Run()
	if err != nil {
		log.Fatal(err)
	}
}
