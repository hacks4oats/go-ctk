package log

import (
	"os"
	"strconv"

	"github.com/charmbracelet/log"
)

func DebugTrace() bool {
	val := os.Getenv("CI_DEBUG_TRACE")
	status, err := strconv.ParseBool(val)
	if err != nil {
		return false
	}
	return status
}

func Debug(msg string, keyvals ...string) {
	log.Debug(msg, keyvals)
}

func Info(msg string, keyvals ...string) {
	log.Info(msg, keyvals)
}
