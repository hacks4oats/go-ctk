package command

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSection(t *testing.T) {
	t.Run("Start", func(t *testing.T) {
		cmd, err := NewCommand([]string{"bash", "-c", "echo hello world"})
		require.NoError(t, err)
		cmd.start()
	})
}
