package command

import (
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"time"
)

const (
	ansiBoldMagentaText = "\x1B[95;1m"
	ansiResetStyle      = "\x1B[0;m"
)

type Command struct {
	stdout       strings.Builder
	stderr       strings.Builder
	name         string
	args         []string
	section      string
	header       string
	autocollapse bool
}

type CommandOpt func(*Command)

func WithHeader(v string) func(*Command) {
	return func(c *Command) { c.header = v }
}

func WithSection(v string) func(*Command) {
	return func(c *Command) { c.section = v }
}

func WithAutoCollapse(v bool) func(*Command) {
	return func(c *Command) { c.autocollapse = v }
}

func NewCommand(args []string, opts ...CommandOpt) (cmd Command, err error) {
	if len(args) == 0 {
		err = fmt.Errorf("not enough command args given")
		return
	}

	cmd.header = ansiBoldMagentaText + "$ " + strings.Join(args, " ") + ansiResetStyle
	cmd.name = args[0]
	cmd.args = args[1:]

	sha := sha1.Sum([]byte(cmd.header))
	cmd.section = fmt.Sprintf("cmd_%x", sha)

	for _, opt := range opts {
		opt(&cmd)
	}

	return
}

func (c Command) Run() (stdout io.Reader, stderr io.Reader, err error) {
	cmd := exec.Command(c.name, c.args...)
	cmd.Stdout = io.MultiWriter(os.Stdout, &c.stdout)
	cmd.Stderr = io.MultiWriter(os.Stderr, &c.stderr)

	c.start()

	if err := cmd.Run(); err != nil {
		return nil, nil, fmt.Errorf("running cmd: %s: %w", cmd.String(), err)
	}

	c.end()

	return strings.NewReader(c.stdout.String()), strings.NewReader(c.stderr.String()), nil
}

func (c Command) start() {
	ts := time.Now().Unix()
	fmt.Printf("\x1B[0Ksection_start:%d:%s[collapsed=%t]\r\x1B[0K%s\n", ts, c.section, c.autocollapse, c.header)
}

func (c Command) end() {
	ts := time.Now().Unix()
	fmt.Printf("\x1B[0Ksection_end:%d:%s\r\x1B[0K\n", ts, c.section)
}
