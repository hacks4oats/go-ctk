package cache

import (
	"context"
	"fmt"
	"io"
	"io/fs"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"time"

	"github.com/hashicorp/go-retryablehttp"
	"github.com/saracen/fastzip"
)

type PackageVariant struct {
	Metadata    PackageMetadata
	OS          string
	Arch        string
	DownloadURL string
	SHA256Hash  string
}

type PackageManifest struct {
	Name     string
	Version  string
	Variants []Variant
}

type Variant struct {
	OS          string
	Arch        string
	DownloadURL string
	SHA256Hash  string
}

func (pkg PackageVariant) FileName() string {
	ext := filepath.Ext(pkg.DownloadURL)
	name := pkg.Metadata.Name +
		"-" + pkg.Metadata.Version +
		"-" + pkg.Metadata.OS +
		"-" + pkg.Metadata.Arch
	if ext != "" {
		name += ext
	}
	return name
}

type PackageType int

const (
	PackageTypeBinary PackageType = iota
	PackageTypeArchive
)

func (pkg PackageVariant) Type() PackageType {
	ext := filepath.Ext(pkg.DownloadURL)
	switch ext {
	case ".zip", ".tar", ".zstd":
		return PackageTypeArchive
	default:
		return PackageTypeBinary
	}
}

type PackageMetadata struct {
	Name    string
	Version string
	OS      string
	Arch    string
}

// A Cache stores packages in the projects generic package registry
// for quick retrieval. It is especially useful when packages
// require compilation or when you wish to respect rate limits.
type Cache struct {
	client    *retryablehttp.Client
	token     string
	baseURL   string
	projectID string
	localDir  string
}

type CacheOpt func(c *Cache)

func WithLocalDir(dir string) CacheOpt {
	return func(c *Cache) { c.localDir = dir }
}

func WithBaseURL(url string) CacheOpt {
	return func(c *Cache) { c.baseURL = url }
}

func WithProjectID(id string) CacheOpt {
	return func(c *Cache) { c.projectID = id }
}

func WithHTTPClient(client *retryablehttp.Client) CacheOpt {
	return func(c *Cache) { c.client = client }
}

func defaultHTTPClient(token string) *retryablehttp.Client {
	client := retryablehttp.NewClient()
	client.HTTPClient.Timeout = 10 * time.Second
	client.HTTPClient.Transport = transport{http.DefaultTransport, token}
	return client
}

type transport struct {
	roundTripper http.RoundTripper
	token        string
}

func (t transport) RoundTrip(r *http.Request) (*http.Response, error) {
	if t.token != "" {
		r.Header.Add("PRIVATE-TOKEN", t.token)
	}
	return t.roundTripper.RoundTrip(r)
}

func NewCache(opts ...CacheOpt) *Cache {
	var cache Cache
	cache.token = os.Getenv("CI_JOB_TOKEN")
	cache.baseURL = os.Getenv("CI_API_V4_URL")
	cache.projectID = os.Getenv("CI_PROJECT_ID")
	cache.localDir = os.Getenv("CI_PROJECT_DIR")

	for _, opt := range opts {
		opt(&cache)
	}

	// Tiny optimization to prevent allocating an http
	// client that will need to be cleaned up by the
	// GC later if not used.
	if cache.client == nil {
		cache.client = defaultHTTPClient(cache.token)
	}

	return &cache
}

// Download fetches a package from the package's download url.
// This function is intended to be used when the package is not
// found in the cache.
func (c *Cache) Download(pkg PackageVariant) (string, error) {
	resp, err := c.client.Get(pkg.DownloadURL)
	if err != nil {
		return "", fmt.Errorf("attempting to download %s: %w", pkg.DownloadURL, err)
	}
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("response code %d received while attempting to download %s", resp.StatusCode, pkg.DownloadURL)
	}
	return "", nil
}

// Fetch retrieves a package from the cache.
func (c *Cache) Fetch(pkg PackageVariant) error {
	url, err := url.JoinPath(c.baseURL, "/projects/", c.projectID, "/packages/generic/", pkg.Metadata.Name, pkg.Metadata.Version, pkg.FileName())
	if err != nil {
		return fmt.Errorf("creating cache url: %w", err)
	}
	resp, err := c.client.Get(url)
	if err != nil {
		return fmt.Errorf("attempting to fetch package from cache %s: %w", url, err)
	}

	defer resp.Body.Close()

	path := filepath.Join(c.localDir, pkg.FileName())
	dst, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0o644)
	if err != nil {
		return fmt.Errorf("opening local file path %s to store pkg: %w", path, err)
	}

	_, err = io.Copy(dst, resp.Body)
	if err != nil {
		return fmt.Errorf("reading pkg into file path %s: %w", path, err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("response code %d received while attempting to get %s", resp.StatusCode, url)
	}
	return nil
}

// Store saves the package in the cache.
func (c *Cache) Store(name, version, pkgPath string) error {
	file := filepath.Base(pkgPath)
	url, err := url.JoinPath(c.baseURL, "/projects/", c.projectID, "/packages/generic/", name, version, file)
	if err != nil {
		return fmt.Errorf("")
	}

	pkg, err := os.Open(pkgPath)
	if err != nil {
		return fmt.Errorf("opening %s: %w", pkgPath, err)
	}
	defer pkg.Close()

	req, err := retryablehttp.NewRequest(http.MethodPut, url, pkg)
	if err != nil {
		return fmt.Errorf("creating retryable http request to store package in cache %s: %w", url, err)
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return fmt.Errorf("attempting to store package in cache %s: %w", url, err)
	}

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("response code %d received while attempting to put %s", resp.StatusCode, url)
	}

	return nil
}

func (c *Cache) Archive(ctx context.Context, src, dst string) error {
	f, err := os.OpenFile(dst, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0o644)
	if err != nil {
		return fmt.Errorf("creating archive output %s: %w", dst, err)
	}
	defer f.Close()

	a, err := fastzip.NewArchiver(f, src)
	if err != nil {
		return fmt.Errorf("creating archiver: %w", err)
	}
	defer a.Close()

	files := make(map[string]os.FileInfo)
	err = filepath.Walk(src, func(path string, info fs.FileInfo, err error) error {
		files[path] = info
		return nil
	})

	if err != nil {
		return fmt.Errorf("walking archive src %s: %w", src, err)
	}

	err = a.Archive(ctx, files)
	if err != nil {
		return fmt.Errorf("archiving %s to %s: %w", src, dst, err)
	}

	return nil
}

func (c *Cache) Extract(ctx context.Context, src, dst string) error {
	if src == "" {
		return fmt.Errorf("cannot extract pkg from empty source")
	}
	if dst == "" {
		return fmt.Errorf("cannot extract pkg to empty destination")
	}

	e, err := fastzip.NewExtractor(src, dst)
	if err != nil {
		return fmt.Errorf("creating extractor: %w", err)
	}
	defer e.Close()

	err = e.Extract(ctx)
	if err != nil {
		return fmt.Errorf("extracting pkg from %s to %s: %w", src, dst, err)
	}
	return nil
}

// Exists verifies if the package can be found in the cache.
func (c *Cache) Exists(name, version string) bool { return false }
